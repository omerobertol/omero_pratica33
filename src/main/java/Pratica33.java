import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 *
 * @author Omero Francisco Bertol (27/09/2016)
 */

public class Pratica33 {

    public static void main(String[] args) {
        Matriz a = new Matriz(3, 3);
        Matriz b = new Matriz(3, 3);
        
        double[][] temp;
        
        // Matriz "a"
        temp = a.getMatriz();
        // 1a. linha
        temp[0][0] = 1.0;
        temp[0][1] = 1.0;
        temp[0][2] = 1.0;
        
        // 2a. linha
        temp[1][0] = 1.0;
        temp[1][1] = 1.0;
        temp[1][2] = 1.0;
        
        // 3a. linha
        temp[2][0] = 1.0;
        temp[2][1] = 1.0;
        temp[2][2] = 1.0;
 
        
        // Matriz "b"
        temp = b.getMatriz();
        // 1a. linha
        temp[0][0] = 2.0;
        temp[0][1] = 2.0;
        temp[0][2] = 2.0;
        
        // 2a. linha
        temp[1][0] = 2.0;
        temp[1][1] = 2.0;
        temp[1][2] = 2.0;
        
        // 3a. linha
        temp[2][0] = 2.0;
        temp[2][1] = 2.0;
        temp[2][2] = 2.0;
        
        System.out.println("Matriz \"a\": " + a);
        System.out.println("Matriz \"b\": " + b);
        
        System.out.println("---------------------------------------------------------");
        System.out.println("\"a\" + \"b\".: " + a.soma(b));
        System.out.println("\"a\" X \"b\".: " + a.prod(b));       
    }
}